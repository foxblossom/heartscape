extends Node2D

# How many segments to divide the ring into.
const BUCKET_COUNT       = 8;
const BUCKETS_PER_RADIAN = BUCKET_COUNT / (2 * PI);

const InputUtility = preload("res://Utility/InputUtility.gd")

export(Enums.EStickControl) var stick  = Enums.EStickControl.Left

signal combo_complete(combo)
signal combo_updated(combo)

# Gesture detection parameters.
export var deadzoneRadius     = 0.2;
export var activationRadius   = 0.8;
export var maxGestureDuration = 1.0;

var _gestureDuration   = 0;
var _lastBucket        = 0;
var _framesInDeadzone  = 0;
var _readyToBegin      = false;
var _patternInProgress = "";

func getStickName():
	return Enums.EStickControl.keys()[stick]

func _process(delta):
	var stick_values = InputUtility.getStickAngleVector(stick)
	var magnitude    = stick_values.length()
	
	if magnitude < deadzoneRadius:
		if _gestureDuration > 0 and _framesInDeadzone > 2:
			EndGesture()
			
		_framesInDeadzone += 1
		_readyToBegin = true
	else:
		_framesInDeadzone = 0
		
		if _readyToBegin and magnitude > activationRadius:
			if _gestureDuration == 0:
				BeginGesture()
			
			_gestureDuration += delta
			if _gestureDuration > maxGestureDuration: 
				print("finish2")
				EndGesture()
			else:
				ProgressGesture(stick_values)
					
func BeginGesture():
	_lastBucket = -1
	_patternInProgress = ""

# Process gesture each frame - check if we needto 
func ProgressGesture(stick_values):
	var angle = atan2(-stick_values.x, -stick_values.y)
	var bucket = int(round(angle * BUCKETS_PER_RADIAN + BUCKET_COUNT / 2)) % BUCKET_COUNT
	
	if bucket != _lastBucket:
		_lastBucket = bucket;
		_patternInProgress += str(bucket)

		emit_signal("combo_updated", _patternInProgress)
		return true
	
	return false
	
	
func EndGesture():
	if _patternInProgress != "":
		emit_signal("combo_complete", _patternInProgress)
		print("Combo: " + _patternInProgress)

	_gestureDuration  = 0
	_readyToBegin      = false
	_patternInProgress = ""
