extends Node2D

signal hp_changed(hp)
signal left_mp_changed(mp)
signal right_mp_changed(mp)

export var HP      = 100
export var LeftMP  = 0
export var RightMP = 0

const MAX_MULTIPLIER    = 4
const ABSORBS_PER_BONUS = 4

var leftStreak  = 0
var rightStreak = 0

func die(): 
	pass

func _on_DefendingPlayer_damage_taken(damage):
	HP -= damage
	leftStreak = 0
	rightStreak = 0
	emit_signal("hp_changed", HP)

	if HP <= 0:
		HP = 0
		die()


func _on_DefendingPlayer_left_absorbed_bullet():
	var bonus = clamp(leftStreak / ABSORBS_PER_BONUS + 1, 1, MAX_MULTIPLIER)
	LeftMP += int(bonus)
	emit_signal("left_mp_changed", LeftMP)
	
	leftStreak += 1
	
	print("Left Absorb - Adding " + str(bonus))

func _on_DefendingPlayer_right_absorbed_bullet():
	var bonus = clamp(rightStreak / ABSORBS_PER_BONUS + 1, 1, MAX_MULTIPLIER)
	RightMP += int(bonus)
	emit_signal("right_mp_changed", RightMP)

	rightStreak += 1
	print("Right Absorb - Adding " + str(bonus))
