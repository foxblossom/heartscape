extends Area2D

export(Enums.EAttackColour) var colour = Enums.EAttackColour.None

export var Speed  = 120
export var Damage = 10

var Shield = preload("res://Battle/Player/Shield.gd")
var Heart = preload("res://Battle/Player/Heart.gd")

func _ready():
	var rand = int(rand_range(0, 3))
	
	if (rand == 1):
		colour = Enums.EAttackColour.LFairy
		get_node("Image").color = PlayerPrefs.LEFT_COLOUR
	if (rand == 2):
		colour = Enums.EAttackColour.RFairy
		get_node("Image").color = PlayerPrefs.RIGHT_COLOUR
		

func _process(delta):
	translate(Vector2(Speed * delta, 0))

func _on_Node2D_body_entered(body):
	if body is Heart:
		body.damage(Damage)
		queue_free()
		
	if body is Shield:
		if colour == Enums.EAttackColour.None or colour == body.colour:
			body.absorb()
			queue_free()
